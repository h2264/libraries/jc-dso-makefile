.PHONY: sample-lint sample-build sample-test sample-package sample-publish


.PHONY: sample-pre-lint sample-do-lint sample-post-lint
sample-pre-lint:
	@echo "Place pre-linting steps here. Perhaps download or install linter tools"
sample-do-lint:
	@echo "Execute lint command"
sample-post-lint:
	@echo "Post linting activities. Ideal place for processing lint related reports for later publishing"
sample-lint: sample-pre-lint sample-do-lint sample-post-lint


.PHONY: sample-pre-build sample-do-build sample-post-build
sample-pre-build:
	@echo "Place pre-build steps here. Perhaps download or install build tools"
sample-do-build:
	@echo "Execute build command"
sample-post-build:
	@echo "Post build activities. Ideal place for processing build related reports for later publishing"
sample-build: sample-pre-build sample-do-build sample-post-build


.PHONY: sample-pre-package sample-do-package sample-post-package
sample-pre-package:
	@echo "Place pre-package steps here. Perhaps download or install package tools"
sample-do-package:
	@echo "Execute package command"
sample-post-package:
	@echo "Post package activities. Ideal place for processing package related reports for later publishing"
sample-package: sample-pre-package sample-do-package sample-post-package


.PHONY: sample-pre-publish sample-do-publish sample-post-publish
sample-pre-publish:
	@echo "Place pre-publish steps here. Perhaps download or install publish tools"
sample-do-publish:
	@echo "Execute publish command"
sample-post-publish:
	@echo "Post publish activities. Ideal place for runtime test against published documents"
sample-publish: sample-pre-publish sample-do-publish sample-post-publish
