.PHONY: dev-deploy

ifeq ($(strip $(DEV_OCI_NAME)),)
  DEV_OCI_NAME=ubuntu
endif

ifeq ($(strip $(DEV_OCI_TAG)),)
  DEV_OCI_TAG=focal-20220404
endif

ifeq ($(strip $(DEV_OCI_BIN)),)
  DEV_OCI_BIN=docker
endif

ifeq ($(strip $(DEV_OCI_MNT)),)
  DEV_OCI_MNT=/mnt
endif

ifeq ($(strip $(DEV_OCI_SH)),)
  DEV_OCI_SH=bash
endif

dev-deploy:
	$(DEV_OCI_BIN) run -ti --rm -v $(PWD):$(DEV_OCI_MNT) -w $(DEV_OCI_MNT) $(DEV_OCI_NAME):$(DEV_OCI_TAG) $(DEV_OCI_SH)




ifeq ($(strip $(TEST)),)
	$(_PRE)=echo
endif