# jc-dso-makefile

DevSecOps Makefile library repository. Modelled after [ska-cicd-makefile](https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile)

## Motivation.
To centralise common tasks of technologies in scope while following a [DevSecOps](https://glossary.cncf.io/devsecops/) workflow. Each makefile is expected to contain the following concepts as targets

| Concept | Comment |
| --- | --- |
| `lint` | Syntax analysis. Promotes code quality |
| `build` | Application build process. |
| `test` | Unit tests. |
| `package` | Application packaging. |
| `publish` | Documentation publishing.|
|||

## System prep
```bash
# While in the root path of a repository, execute the following:
git submodule add https://gitlab.com/h2264/libraries/jc-dso-makefile.git .make
git submodule add git@gitlab.com:h2264/libraries/jc-dso-makefile.git .make

# Syncronise submodule
git submodule update --init --force --recursive
```

## Usage
```bash
# Generate Makefile in repository's root directory
cat << 'EOF' >> Makefile
# include base makefile
include .make/base.mk

# include makefiles to be inherited
include .make/sample.mk

# include repo specific override residing in current repository
-include Override.mk

# include workstation specific targets
-include WorkstationTargets.mk
EOF

# Generate .gitignore file with workstation specific files
cat << 'EOF' >> .gitignore
# Ignore temporary files
*.tmp

# Ignore workstation local make targets
WorkstationTargets.mk
EOF
```

## CICD integration
### System prep
### Repository Layout
### Payloads
### Usage

