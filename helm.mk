.PHONY: helm-install helm-lint helm-logs helm-render helm-status

OCI_IMAGE_SCRIPT_DIR := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))
HELM_SUPPORT := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))/support/helm.sh
OCI_REGISTRY_REPO=library
OCI_PROXY_REPO=dockerhub
# TAG=$(shell . $(OCI_SUPPORT); RELEASE_CONTEXT_DIR=$(RELEASE_CONTEXT_DIR) setContextHelper; getTag)
TAG=$(shell cat version.txt)
SHELL=/usr/bin/env bash

# User defined variables
OCI_HOST = harbor.prd.k3s.vm
OCI_REGISTRY_HOST ?= ## OCI Image Registry
ifeq ($(strip $(OCI_REGISTRY_HOST)),)
  OCI_REGISTRY_HOST = $(OCI_HOST)/$(OCI_REGISTRY_REPO)
endif
OCI_IMAGE_BUILD_CONTEXT ?= .  ## Image build context directory, relative to ./images/<image dir> for multiple images, gets replaced by `$(PWD)` for single images where Dockerfile is in the root folder. Don't use `.` to provide root folder for multiple images, use `$(PWD)`.
HELM_CHARTS_FILE_PATH ?= Charts.yaml ## Image recipe file
HELM_CHART_DIRS := $(shell if [ -d charts ]; then cd charts; for name in $$(ls); do if [ -d $$name ]; then echo $$name; fi done else echo $(PROJECT_NAME);  fi;)
HELM_CHARTS ?= $(HELM_CHART_DIRS) ## Images to lint and build
HELM_CHARTS_TO_PUBLISH ?= $(HELM_CHARTS) ## Images to publish
HELM_CHART ?= $(PROJECT_NAME) ## Default Image (from /images/<HELM_CHART dir>)
OCI_HELM_BUILDER ?= docker ## Image builder eg: docker, or podman
OCI_BUILD_ADDITIONAL_ARGS ?= ## Additional build argument string
OCI_HELM_IMAGE  ?= $(OCI_HOST)/$(OCI_PROXY_REPO)/alpine/helm:latest
OCI_HELM_CMD ?= # Helm command set per target
OCI_HELM_FLAGS ?= ## CLI Flags allowing interaction to the local OCI registry
HELM_SKIP_PUSH ?= "true" ## OCI Skip the push

.PHONY: helm-pre-lint helm-do-lint helm-post-lint
helm-pre-lint:
helm-do-lint:
	@. $(HELM_SUPPORT) ; OCI_HELM_BUILDER=$(OCI_HELM_BUILDER) \
	OCI_HELM_IMAGE=$(OCI_HELM_IMAGE ) \
	HELM_CHARTS_FILE_PATH=$(HELM_CHARTS_FILE_PATH) \
	OCI_HELM_FLAGS=$(OCI_HELM_FLAGS) \
	helmChartLint "$(HELM_CHARTS)"
helm-post-lint: helm-do-render
helm-lint: helm-pre-lint helm-do-lint helm-post-lint


.PHONY: helm-pre-install helm-do-install helm-post-install
helm-install: helm-pre-install helm-do-install helm-post-install


.PHONY: helm-pre-render helm-do-render helm-post-render
helm-pre-render:
helm-do-render:
	@. $(HELM_SUPPORT) ; OCI_HELM_BUILDER=$(OCI_HELM_BUILDER) \
	OCI_HELM_IMAGE=$(OCI_HELM_IMAGE) \
	HELM_CHARTS_FILE_PATH=$(HELM_CHARTS_FILE_PATH) \
	OCI_HELM_FLAGS=$(OCI_HELM_FLAGS) \
	helmChartRender "$(HELM_CHARTS)"
helm-post-render:
helm-render: helm-pre-render helm-do-render helm-post-render


.PHONY: helm-pre-status helm-do-status helm-post-status
helm-status: helm-pre-status helm-do-status helm-post-status


.PHONY: helm-pre-logs helm-do-logs helm-post-logs
helm-logs: helm-pre-logs helm-do-logs helm-post-logs