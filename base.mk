# Centralised include makefile for common targets
include .make/make.mk

# NOTIFICATION TARGET
CICD_NOTIFICATIONS ?= slack.mk # slack, discord, etc
include .make/$(CICD_NOTIFICATIONS)