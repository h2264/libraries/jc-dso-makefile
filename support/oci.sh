#!/usr/bin/env bash

# Shellscript support function file for OCI Images Make targets

# Lint OCI Image Dockerfiles in images/<subdirs>
function ociImageLint() {
	if [ -z "$1" ] ; then
		echo "ociImageLint: Missing OCI_IMAGES"
        exit 1
	else
        OCI_IMAGES="$1"
	fi

	if [ -f Dockerfile ] ; then
        echo "ociImageLint: Linting: ${OCI_IMAGE_FILE_PATH}"
        ${OCI_BUILDER} run --rm -i ${OCI_LINTER} hadolint --failure-threshold error - < \
            ${OCI_IMAGE_FILE_PATH}
            ret_val=$?
            if [ $ret_val -ne 0 ]; then
                echo "ociImageLint: Linting of ${OCI_IMAGES} failed - aborting!"
                exit 1;
            fi
    else
        for IMAGE in ${OCI_IMAGES}; do
            echo "ociImageLint: Linting: ${IMAGE}"
            ${OCI_BUILDER} run --rm -i ${OCI_LINTER} hadolint --failure-threshold error - < \
            images/${IMAGE}/${OCI_IMAGE_FILE_PATH}
            ret_val=$?
            if [ $ret_val -ne 0 ]; then
                echo "ociImageLint: Linting of ${IMAGE} failed - aborting!"
                exit 1;
            fi
        done
    fi
    echo "ociImageLint: done."
}

# Build OCI Image
function ociImageBuild() {
	if [ -z "$1" ] ; then
		echo "ociImageBuild: Missing OCI_IMAGE"
        exit 1
	else
        OCI_IMAGE="$1"
    fi
	if [ -z "$OCI_TAG" ] ; then
		echo "ociImageBuild: Missing OCI_TAG"
        OCI_TAG="latest"
    fi
    # Check if this is a dev build
    if [[ "${OCI_REGISTRY_HOST}" == registry.gitlab.com* ]] || [[ -z "${OCI_REGISTRY_HOST}" ]]; then
        echo "ociImageBuild: Gitlab registry is used to publish as the image is for development use"
        OCI_TAG=$OCI_TAG-dev.c$CI_COMMIT_SHORT_SHA  #"-" is used as "+" causes the docker building process to fail    
    fi

    if [ -z "$OCI_IMAGE_BUILDER" ]
    then
        OCI_IMAGE_BUILDER=podman
    fi

    if [ -z "$OCI_REGISTRY_HOST" ]
    then
        echo "ociImageBuild: OCI_REGISTRY_HOST is not set"
    fi

    if [ -z "$OCI_IMAGE_BUILD_CONTEXT" ]
    then
        OCI_IMAGE_BUILD_CONTEXT=.
    fi
	if [ "." = "${OCI_IMAGE_BUILD_CONTEXT}" ]; then
		OCI_IMAGE_BUILD_CONTEXT="images/${OCI_IMAGE}"
	fi

    if [ -z "$OCI_IMAGE_FILE_PATH" ]
    then
        OCI_IMAGE_FILE_PATH=Dockerfile
    fi

    # Collect LABELs from CI environment variables
    ENV_LABELS="CI_COMMIT_AUTHOR CI_COMMIT_REF_NAME CI_COMMIT_REF_SLUG"
    ENV_LABELS+=" CI_COMMIT_SHA CI_COMMIT_SHORT_SHA CI_COMMIT_TIMESTAMP"
    ENV_LABELS+=" CI_JOB_ID CI_JOB_URL"
    ENV_LABELS+=" CI_PIPELINE_ID CI_PIPELINE_IID CI_PIPELINE_URL"
    ENV_LABELS+=" CI_PROJECT_ID CI_PROJECT_PATH_SLUG CI_PROJECT_URL"
    ENV_LABELS+=" CI_RUNNER_ID CI_RUNNER_REVISION CI_RUNNER_TAGS"
    ENV_LABELS+=" GITLAB_USER_NAME GITLAB_USER_EMAIL GITLAB_USER_LOGIN"
    ENV_LABELS+=" GITLAB_USER_ID"
    for KEY in $ENV_LABELS; do
      VALUE=$(eval "echo \"\$${KEY}\"")
      if [ -n "$VALUE" ]; then
        if [ -z "$OCI_LABELS" ]; then
          OCI_LABELS="--label $KEY='$VALUE'"
        else
          OCI_LABELS="$OCI_LABELS --label $KEY='$VALUE'"
        fi
      fi
    done

    # Build the image
    OCI_BUILD_COMMAND="$OCI_BUILDER build $OCI_LABELS -t $OCI_REGISTRY_HOST/$OCI_IMAGE:$OCI_TAG -f $OCI_IMAGE_FILE_PATH $OCI_BUILD_ADDITIONAL_ARGS $OCI_IMAGE_BUILD_CONTEXT"
    echo "ociImageBuild: Build command: $OCI_BUILD_COMMAND"
    bash -c "$OCI_BUILD_COMMAND"
    status=$?
    if ! [[ $status == 0 ]]; then
        exit $status
    fi

    echo "ociImageBuild: done."
}

# Inspect OCI Image
function ociImageInspect() {
	if [ -z "$1" ] ; then
		echo "ociImageInspect: Missing OCI_IMAGE"
        exit 1
	else
        OCI_IMAGE="$1"
    fi
	if [ -z "$OCI_TAG" ] ; then
		echo "ociImageBuild: Missing OCI_TAG"
        OCI_TAG="latest"
    fi
    # Check if this is a dev build
    if [[ "${OCI_REGISTRY_HOST}" == registry.gitlab.com* ]] || [[ -z "${OCI_REGISTRY_HOST}" ]]; then
        echo "ociImageBuild: Gitlab registry is used to publish as the image is for development use"
        OCI_TAG=$OCI_TAG-dev.c$CI_COMMIT_SHORT_SHA  #"-" is used as "+" causes the docker building process to fail    
    fi

    if [ -z "$OCI_IMAGE_BUILDER" ]
    then
        OCI_IMAGE_BUILDER=podman
    fi

    if [ -z "$OCI_REGISTRY_HOST" ]
    then
        echo "ociImageInspect: OCI_REGISTRY_HOST is not set"
    fi

    # Inspect the image
    OCI_INSPECT_COMMAND="$OCI_BUILDER inspect $OCI_REGISTRY_HOST/$OCI_IMAGE:$OCI_TAG"
    echo "ociImageInspect: Inspect command: $OCI_INSPECT_COMMAND"
    bash -c "$OCI_INSPECT_COMMAND"
    status=$?
    if ! [[ $status == 0 ]]; then
        exit $status
    fi

    echo "ociImageInspect: done."
}

# Scan OCI Image
function ociImageScan() {
	if [ -z "$1" ] ; then
		echo "ociImageScan: Missing OCI_IMAGES"
        exit 1
	else
        OCI_IMAGES="$1"
	fi
	if [ -z "$OCI_TAG" ] ; then
		echo "ociImageBuild: Missing OCI_TAG"
        OCI_TAG="latest"
    fi
    # Check if this is a dev build
    if [[ "${OCI_REGISTRY_HOST}" == registry.gitlab.com* ]] || [[ -z "${OCI_REGISTRY_HOST}" ]]; then
        echo "ociImageBuild: Gitlab registry is used to publish as the image is for development use"
        OCI_TAG=$OCI_TAG-dev.c$CI_COMMIT_SHORT_SHA  #"-" is used as "+" causes the docker building process to fail    
    fi
    if [ -z "$OCI_SCANNER_FLAGS" ]
    then
        echo "ociImageScan: OCI_SCANNER_FLAGS is not set"
    fi

	if [ -f Dockerfile ] ; then
        echo "ociImageScan: Scanning: ${OCI_IMAGE}"
        ${OCI_BUILDER} run ${OCI_SCANNER_FLAGS} --rm -i ${OCI_SCANNER} image $OCI_REGISTRY_HOST/${IMAGE}:${TAG}
            ret_val=$?
            if [ $ret_val -ne 0 ]; then
                echo "ociImageScan: Scanning of ${OCI_SCANNER} failed - aborting!"
                exit 1;
            fi
    else
        for IMAGE in ${OCI_IMAGES}; do
            echo "ociImageScan: Scanning: ${IMAGE}"
            OCI_BUILD_COMMAND="${OCI_BUILDER} run ${OCI_SCANNER_FLAGS} --rm -i ${OCI_SCANNER} image $OCI_REGISTRY_HOST/${IMAGE}:${TAG}"
            echo "ociImageScan: Scan command: $OCI_BUILD_COMMAND"
            bash -c "$OCI_BUILD_COMMAND"
            ret_val=$?
            if [ $ret_val -ne 0 ]; then
                echo "ociImageScan: Scanning of $OCI_REGISTRY_HOST/${IMAGE}:${TAG} failed - aborting!"
                exit 1;
            fi
        done
    fi
    echo "ociImageScan: done."
}

# Publish OCI Image
function ociImagePublish() {
	if [ -z "$1" ] ; then
		echo "ociImagePublish: Missing OCI_IMAGES"
        exit 1
	else
        OCI_IMAGES="$1"
	fi
    if [ -z "$OCI_REGISTRY_HOST" ]
    then
        echo "ociImagePublish: OCI_REGISTRY_HOST is not set"
    fi
	if [ -z "$OCI_TAG" ] ; then
		echo "ociImageBuild: Missing OCI_TAG"
        OCI_TAG="latest"
    fi
    # Check if this is a dev build
    if [[ "${OCI_REGISTRY_HOST}" == registry.gitlab.com* ]] || [[ -z "${OCI_REGISTRY_HOST}" ]]; then
        echo "ociImageBuild: Gitlab registry is used to publish as the image is for development use"
        OCI_TAG=$OCI_TAG-dev.c$CI_COMMIT_SHORT_SHA  #"-" is used as "+" causes the docker building process to fail    
    fi

    echo "ociImagePublish: OCI_SKIP_PUSH: [${OCI_SKIP_PUSH}]"
    if [[ "$OCI_SKIP_PUSH" = "true" ]] ; then
        echo "ociImagePublish: Skipped."
    else
        if [ -f Dockerfile ] ; then
            echo "ociImagePublish: Publishing: ${OCI_IMAGE}"
            OCI_PUBLISH_COMMAND="${OCI_BUILDER} push $OCI_REGISTRY_HOST/${OCI_IMAGE}:${OCI_TAG}"
            bash -c "$OCI_PUBLISH_COMMAND"
            ret_val=$?
            if [ $ret_val -ne 0 ]; then
                echo "ociImagePublish: Publishing of $OCI_REGISTRY_HOST/${IMAGE}:${OCI_TAG} failed - aborting!"
                exit 1;
            fi
        else
            for IMAGE in ${OCI_IMAGES}; do
                echo "ociImagePublish: Publishing: ${IMAGE}"
                OCI_PUBLISH_COMMAND="${OCI_BUILDER} push $OCI_REGISTRY_HOST/${IMAGE}:${OCI_TAG}"
                echo "ociImagePublish: Publish command: $OCI_PUBLISH_COMMAND"
                bash -c "$OCI_PUBLISH_COMMAND"
                ret_val=$?
                if [ $ret_val -ne 0 ]; then
                    echo "ociImagePublish: Publishing of $OCI_REGISTRY_HOST/${IMAGE}:${OCI_TAG} failed - aborting!"
                    exit 1;
                fi
            done
        fi
        echo "ociImagePublish: done."
    fi    
}

# Clean OCI Images
function ociImageClean() {
	if [ -z "$1" ] ; then
		echo "ociImageClean: Missing OCI_IMAGES"
        exit 1
	else
        OCI_IMAGES="$1"
	fi
    if [ -z "$OCI_REGISTRY_HOST" ]
    then
        echo "ociImageClean: OCI_REGISTRY_HOST is not set"
    fi
	if [ -z "$OCI_TAG" ] ; then
		echo "ociImageBuild: Missing OCI_TAG"
        OCI_TAG="latest"
    fi
    # Check if this is a dev build
    if [[ "${OCI_REGISTRY_HOST}" == registry.gitlab.com* ]] || [[ -z "${OCI_REGISTRY_HOST}" ]]; then
        echo "ociImageBuild: Gitlab registry is used to publish as the image is for development use"
        OCI_TAG=$OCI_TAG-dev.c$CI_COMMIT_SHORT_SHA  #"-" is used as "+" causes the docker building process to fail    
    fi

    if [ -f Dockerfile ] ; then
        echo "ociImageClean: Cleaning: ${OCI_IMAGE}"
        OCI_CLEAN_COMMAND="${OCI_BUILDER} rmi $OCI_REGISTRY_HOST/${OCI_IMAGE}:${OCI_TAG}"
        bash -c "$OCI_CLEAN_COMMAND"
        ret_val=$?
        if [ $ret_val -ne 0 ]; then
            echo "ociImageClean: Cleaning of $OCI_REGISTRY_HOST/${IMAGE}:${OCI_TAG} failed - aborting!"
            exit 1;
        fi
    else
        for IMAGE in ${OCI_IMAGES}; do
            echo "ociImageClean: Cleaning: ${IMAGE}"
            OCI_CLEAN_COMMAND="${OCI_BUILDER} rmi $OCI_REGISTRY_HOST/${IMAGE}:${OCI_TAG}"
            echo "ociImageClean: Cleaning command: $OCI_CLEAN_COMMAND"
            bash -c "$OCI_CLEAN_COMMAND"
            ret_val=$?
            if [ $ret_val -ne 0 ]; then
                echo "ociImageClean: Cleaning of $OCI_REGISTRY_HOST/${IMAGE}:${OCI_TAG} failed - aborting!"
                exit 1;
            fi
        done
    fi
    echo "ociImageClean: done."
}