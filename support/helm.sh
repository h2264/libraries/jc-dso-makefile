#!/usr/bin/env bash

# Shellscript support function file for Helm Charts Make targets

# Lint Helm chart in charts/<subdirs>
function helmChartLint() {
	if [ -z "$1" ] ; then
		echo "helmChartLint: Missing HELM_CHARTS"
        exit 1
	else
        HELM_CHARTS="$1"
	fi
    if [ -z "$OCI_HELM_FLAGS" ]
    then
        echo "helmChartLint: OCI_HELM_FLAGS is not set"
    fi

	if [ -f Chart.yaml ] ; then
        echo "helmChartLint: Linting: ${HELM_CHARTS_FILE_PATH}"
        ${OCI_HELM_BUILDER} run --rm -i ${OCI_HELM_CMD} lint \
            ${HELM_CHARTS_FILE_PATH}
            ret_val=$?
            if [ $ret_val -ne 0 ]; then
                echo "helmChartLint: Linting of ${HELM_CHARTS_FILE_PATH} failed - aborting!"
                exit 1;
            fi
    else
        for CHART in ${HELM_CHARTS}; do
            echo "helmChartLint: Linting: ${CHART}"
            HELM_LINT_COMMAND="$OCI_HELM_BUILDER run --rm \
                ${OCI_HELM_FLAGS} \
                -v ${PWD}:/mnt/${CHART} -w /mnt/${CHART} \
                -i ${OCI_HELM_CMD} lint \
                charts/${CHART}/"
            echo "helmChartLint: Lint command: $HELM_LINT_COMMAND"
            bash -c "$HELM_LINT_COMMAND"
            ret_val=$?
            if [ $ret_val -ne 0 ]; then
                echo "helmChartLint: Linting of ${CHART} failed - aborting!"
                exit 1;
            fi
        done
    fi
    echo "helmChartLint: done."
}

# Render Helm chart in charts/<subdirs> with values charts/<subdirs>/values.yaml
function helmChartRender() {
	if [ -z "$1" ] ; then
		echo "helmChartRender: Missing HELM_CHARTS"
        exit 1
	else
        HELM_CHARTS="$1"
	fi
    if [ -z "$OCI_HELM_FLAGS" ]
    then
        echo "helmChartRender: OCI_HELM_FLAGS is not set"
    fi

	if [ -f Chart.yaml ] ; then
        echo "helmChartRender: Rendering: ${HELM_CHARTS_FILE_PATH}"
        ${OCI_HELM_BUILDER} run --rm -i ${OCI_HELM_CMD} values . \
            ${HELM_CHARTS_FILE_PATH}
            ret_val=$?
            if [ $ret_val -ne 0 ]; then
                echo "helmChartRender: Rendering of ${HELM_CHARTS_FILE_PATH} failed - aborting!"
                exit 1;
            fi
    else
        for CHART in ${HELM_CHARTS}; do
            echo "helmChartRender: Rendering: ${CHART}"
            HELM_LINT_COMMAND="$OCI_HELM_BUILDER run --rm \
                ${OCI_HELM_FLAGS} \
                -v ${PWD}:/mnt/${CHART} -w /mnt/${CHART} \
                $OCI_HELM_IMAGE \
                ${OCI_HELM_CMD} template \
                charts/${CHART}/ > Workstation.manifest.yaml"
            echo "helmChartRender: Render command: $HELM_LINT_COMMAND"
            bash -c "$HELM_LINT_COMMAND"
            ret_val=$?
            if [ $ret_val -ne 0 ]; then
                echo "helmChartRender: Rendering of ${CHART} failed - aborting!"
                exit 1;
            fi
        done
    fi
    echo "helmChartLint: done."
}

# Execute helm against Helm chart in charts/<subdirs>
function helmChartExec() {
	if [ -z "$1" ] ; then
		echo "helmChartRender: Missing HELM_CHARTS"
        exit 1
	else
        HELM_CHARTS="$1"
	fi
    if [ -z "$OCI_HELM_FLAGS" ]
    then
        echo "helmChartRender: OCI_HELM_FLAGS is not set"
    fi
    if [ -z "$OCI_HELM_CMD" ]
    then
        echo "helmChartRender: OCI_HELM_FLAGS is not set"
    fi

	if [ -f Chart.yaml ] ; then
        echo "helmChartRender: Rendering: ${HELM_CHARTS_FILE_PATH}"
        ${OCI_HELM_BUILDER} run --rm -i ${OCI_HELM_CMD} values . \
            ${HELM_CHARTS_FILE_PATH}
            ret_val=$?
            if [ $ret_val -ne 0 ]; then
                echo "helmChartRender: Linting of ${HELM_CHARTS_FILE_PATH} failed - aborting!"
                exit 1;
            fi
    else
        for CHART in ${HELM_CHARTS}; do
            echo "helmChartRender: Linting: ${CHART}"
            HELM_LINT_COMMAND="$OCI_HELM_BUILDER run --rm \
                ${OCI_HELM_FLAGS} \
                -v ${PWD}:/mnt/${CHART} -w /mnt/${CHART} \
                -i ${OCI_HELM_CMD} template \
                charts/${CHART}/"
            echo "helmChartRender: Render command: $HELM_LINT_COMMAND"
            bash -c "$HELM_LINT_COMMAND"
            ret_val=$?
            if [ $ret_val -ne 0 ]; then
                echo "helmChartRender: Rendering of ${CHART} failed - aborting!"
                exit 1;
            fi
        done
    fi
    echo "helmChartLint: done."
}