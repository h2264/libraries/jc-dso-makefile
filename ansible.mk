.PHONY:	ansible-systemprep ansible-lint ansible-test ansible-exec ansible-package ansible-publish

# GLOBAL ENV VARS
_ANSIBLE_VENV=.venv
_ANSIBLE_SRC=./src

.PHONY: ansible-pre-systemprep ansible-do-systemprep ansible-post-systemprep
_ANSIBLE_REQUIREMENTS=./dependencies/pip.txt
ansible-pre-systemprep:
	rm -rf $(_ANSIBLE_VENV)
	python3 -m venv $(_ANSIBLE_VENV)
	$(call venv_exec,$(_ANSIBLE_VENV),pip install --upgrade pip)

ansible-do-systemprep:
	$(call venv_exec,$(_ANSIBLE_VENV),pip install -r $(_ANSIBLE_REQUIREMENTS))

ansible-post-systemprep:
	$(call venv_exec,$(_ANSIBLE_VENV),which ansible)

ansible-systemprep: ansible-pre-systemprep ansible-do-systemprep ansible-post-systemprep


.PHONY: ansible-pre-lint ansible-do-lint ansible-post-lint
_ANSIBLE_LINT_YAML_RULES=-d "{extends: relaxed, rules: {line-length: {max: 350}}}"
_ANSIBLE_LINT_YAML_CONF=.config/ansible-lint.yaml
ansible-pre-lint:
	$(call venv_exec,$(_ANSIBLE_VENV),which yamllint)
	$(call venv_exec,$(_ANSIBLE_VENV),which ansible-lint)
ansible-do-lint:
	-$(call venv_exec,$(_ANSIBLE_VENV),yamllint $(_ANSIBLE_LINT_YAML_RULES) $(_ANSIBLE_SRC))
	-$(call venv_exec,$(_ANSIBLE_VENV),ansible-lint -c $(_ANSIBLE_LINT_YAML_CONF) $(_ANSIBLE_SRC))
ansible-post-lint:
ansible-lint: ansible-pre-lint ansible-do-lint ansible-post-lint


.PHONY: ansible-pre-test ansible-do-test ansible-post-test
ansible-pre-test:
ansible-do-test:
ansible-post-test:
ansible-lint: ansible-pre-test ansible-do-test ansible-post-test


.PHONY: ansible-pre-exec ansible-do-exec ansible-post-exec
_ANSIBLE_EXEC_ENVVARS=
_ANSIBLE_EXEC_INVENTORY=''
_ANSIBLE_EXEC_PLAYBOOK=''
_ANSIBLE_EXEC_TAGS='all'
ansible-pre-exec:
ansible-do-exec:
	$(call venv_exec,$(_ANSIBLE_VENV),ansible-playbook -i $(_ANSIBLE_EXEC_INVENTORY) $(_ANSIBLE_EXEC_ENVVARS) --tags=$(_ANSIBLE_EXEC_TAGS) $(_ANSIBLE_SRC)/$(_ANSIBLE_EXEC_PLAYBOOK))
ansible-post-exec:
ansible-exec: ansible-pre-exec ansible-do-exec ansible-post-exec


.PHONY: ansible-pre-package ansible-do-package ansible-post-package
ansible-pre-package:
ansible-do-package:
ansible-post-package:
ansible-lint: ansible-pre-package ansible-do-package ansible-post-package


.PHONY: ansible-pre-publish ansible-do-publish ansible-post-publish
ansible-pre-publish:
ansible-do-publish:
ansible-post-publish:
ansible-lint: ansible-pre-publish ansible-do-publish ansible-post-publish

# VENV FUNCTIONS
define venv_exec
	$(if [ ! -f "$($(1)/bin/activate)" ], python3 -m venv $(1))
	( \
    	source $(1)/bin/activate; \
    	$(2) \
	)
endef
