.PHONY: terraform-systemprep terraform-init terraform-lint terraform-test terraform-apply terraform-clean terraform-package terraform-publish

# GLOBAL VARS
_TF_OCI=docker
_TF_IMAGE=hashicorp/terraform
_TF_TAG=latest
_TF_DATA=/data
_TF_SRC=$(PWD)/src
_TF_VAR_FLAG=--var-file
_TF_VAR_FILE=
_TF_PARAMETERS=$(_TF_VAR_FLAG)=$(_TF_VAR_FILE)

.PHONY: terraform-pre-systemprep terraform-do-systemprep terraform-post-systemprep
terraform-pre-systemprep:
	$(_TF_OCI) rmi -f $(_TF_IMAGE):$(_TF_TAG)
terraform-do-systemprep:
	$(_TF_OCI) pull $(_TF_IMAGE):$(_TF_TAG)
terraform-post-systemprep:
	$(_TF_OCI) images $(_TF_IMAGE):$(_TF_TAG)
terraform-systemprep: terraform-pre-systemprep terraform-do-systemprep terraform-post-systemprep


.PHONY: terraform-pre-init terraform-do-init terraform-post-init
terraform-pre-init: terraform-systemprep
	@rm -rf .terraform
terraform-do-init:
	$(call oci_terraform,init)
terraform-post-init:
terraform-init: terraform-pre-init terraform-do-init terraform-post-init


terraform-lint:


.PHONY: terraform-pre-test terraform-do-test terraform-post-test
terraform-pre-test:
	$(call oci_terraform,refresh,$(_TF_PARAMETERS))
terraform-do-test:
	$(call oci_terraform,validate)
terraform-post-test:
	$(call oci_terraform,plan,$(_TF_PARAMETERS))
terraform-test: terraform-pre-test terraform-do-test terraform-post-test


.PHONY: terraform-pre-apply terraform-do-apply terraform-post-apply
terraform-pre-apply: terraform-test
terraform-do-apply:
	$(call oci_terraform,apply --auto-approve,$(_TF_PARAMETERS))
terraform-post-apply:
	$(call oci_terraform,show)
terraform-apply: terraform-pre-apply terraform-do-apply terraform-post-apply


.PHONY: terraform-pre-clean terraform-do-clean terraform-post-clean
terraform-pre-clean: terraform-test
terraform-do-clean:
	$(call oci_terraform,destroy --auto-approve,$(_TF_PARAMETERS))
terraform-post-clean:
	$(call oci_terraform,show)
terraform-clean: terraform-pre-clean terraform-do-clean terraform-post-clean

terraform-package:
terraform-publish:

# 1: terraform command. E.g. init, apply, plan, validate, show
# 2: terraform parameters
define oci_terraform
	@$(_TF_OCI) run -it --rm -v $(_TF_SRC):$(_TF_DATA) $(_TF_IMAGE):$(_TF_TAG) -chdir=$(_TF_DATA) $(1) $(2)
endef