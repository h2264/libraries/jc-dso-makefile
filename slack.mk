.PHONY: slack-ci-exit test

_SLACK_PAYLOAD_FILE=/tmp/payload.json
_SLACK_URL ?= https://hooks.slack.com/services
_SLACK_URI ?= 
slack-ci:
# $(call _payload.exec,$(_SLACK_PAYLOAD_FILE),$(_payload.ci.test))
	$(call _payload.exec)


define _payload.exec
@echo "CI_NAME: ${CI_NAME}"
@echo "CI_URL: ${CI_URL}"
@echo "CI_STATUS: ${CI_STATUS}"
@curl -X POST -H 'Content-type: application/json' \
--data '{"blocks": [{"type": "section","text": {"type": "mrkdwn","text":\
"Hello Devops Engineers!\n\
A pipeline has been triggerd.\n\
Details are as follows:"\
}},{"type": "divider"},{"type": "section","text":{"type": "mrkdwn","text":\
"CI_NAME: ${CI_NAME}\n\
CI_URL: ${CI_URL}\n\
CI_STATUS: ${CI_STATUS}\n\
GIT_URL: ${GIT_URL}\n\
GIT_BRANCH: ${GIT_BRANCH}\n\
GIT_COMMIT: ${GIT_COMMIT}\n\
"}}]}' \
${_SLACK_URL}/${_SLACK_URI}
endef

# TESTED ONLY WORKS WITH GMAKE 4.3
test:
	$(call _payload.create,${_SLACK_PAYLOAD_FILE},${_payload.ci.post})
	$(call _payload.exec.test)
	$(call _payload.delete,${_SLACK_PAYLOAD_FILE})

define _payload.exec.test
curl -X POST -H 'Content-type: application/json' \
--data-binary "@${_SLACK_PAYLOAD_FILE}" \
${_SLACK_URL}/${_SLACK_URI}
endef

define _payload.ci.post
{
"blocks": [{
"type": "section",
"text": {
"type": "mrkdwn",
"text": "Hello Devops Engineers!\nThe ci ${CI_NAME} has been triggerd.\n It exited with the following details:"
}},{
"type": "divider"
},{
"type": "section",
"text": {
"type": "mrkdwn",
"text": "PIPELINE: ${CI_NAME}\nURL: ${CI_URL}\nSTATUS: ${CI_STATUS}"
}}]}
endef


define _payload.create
@echo "INIT FILE: $1"
@echo "$(_payload.ci.test)"
$(file >$(1),$(2))
cat $1
endef


define _payload.delete
@echo "DELETE FILE: ${1}"
rm -f ${1}
endef
