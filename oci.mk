.PHONY:	oci-lint oci-build oci-scan oci-exec oci-publish oci-clean

OCI_IMAGE_SCRIPT_DIR := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))
OCI_SUPPORT := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))/support/oci.sh
OCI_REGISTRY_REPO=library
OCI_PROXY_REPO=dockerhub
# TAG=$(shell . $(OCI_SUPPORT); RELEASE_CONTEXT_DIR=$(RELEASE_CONTEXT_DIR) setContextHelper; getTag)
OCI_TAG=$(shell cat version.txt)
SHELL=/usr/bin/env bash

# User defined variables
OCI_HOST = harbor.prd.k3s.vm
OCI_REGISTRY_HOST ?= ## OCI Image Registry
ifeq ($(strip $(OCI_REGISTRY_HOST)),)
  OCI_REGISTRY_HOST = $(OCI_HOST)/$(OCI_REGISTRY_REPO)
endif
OCI_IMAGE_BUILD_CONTEXT ?= .  ## Image build context directory, relative to ./images/<image dir> for multiple images, gets replaced by `$(PWD)` for single images where Dockerfile is in the root folder. Don't use `.` to provide root folder for multiple images, use `$(PWD)`.
OCI_IMAGE_FILE_PATH ?= Dockerfile ## Image recipe file
OCI_IMAGE_DIRS := $(shell if [ -d images ]; then cd images; for name in $$(ls); do if [ -d $$name ]; then echo $$name; fi done else echo $(PROJECT_NAME);  fi;)
OCI_IMAGES ?= $(OCI_IMAGE_DIRS) ## Images to lint and build
OCI_IMAGES_TO_PUBLISH ?= $(OCI_IMAGES) ## Images to publish
OCI_IMAGE ?= $(PROJECT_NAME) ## Default Image (from /images/<OCI_IMAGE dir>)
OCI_BUILDER ?= docker ## Image builder eg: docker, or podman
OCI_BUILD_ADDITIONAL_ARGS ?= ## Additional build argument string
OCI_LINTER ?= $(OCI_HOST)/$(OCI_PROXY_REPO)/hadolint/hadolint:latest
OCI_SCANNER ?= $(OCI_HOST)/$(OCI_PROXY_REPO)/aquasec/trivy:latest
OCI_SCANNER_FLAGS ?= "-v /var/run/docker.sock:/var/run/docker.sock" ## CLI Flags allowing interaction to the local OCI registry
OCI_SKIP_PUSH ?= "true" ## OCI Skip the push
OCI_BUILD_FLAGS = export OCI_IMAGE_BUILD_CONTEXT=$(OCI_IMAGE_BUILD_CONTEXT); \
	export OCI_IMAGE_FILE_PATH=images/$(strip $(OCI_IMAGE))/$(OCI_IMAGE_FILE_PATH); \
	if [[ -f Dockerfile ]]; then \
		echo "This is a oneshot OCI Image project with Dockerfile in the root OCI_IMAGE_BUILD_CONTEXT=$(PWD)"; \
		export OCI_IMAGE_BUILD_CONTEXT=$(PWD); \
		export OCI_IMAGE_FILE_PATH=$(OCI_IMAGE_FILE_PATH); \
	fi; \
	PROJECT_NAME=$(PROJECT_NAME) \
	OCI_REGISTRY_HOST=$(OCI_REGISTRY_HOST) \
	OCI_BUILDER=$(OCI_BUILDER) \
	OCI_TAG=$(OCI_TAG) \
	OCI_IMAGE=$(strip $(OCI_IMAGE)) \
	TAG=$(TAG)

.PHONY: oci-pre-lint oci-do-lint oci-post-lint
oci-pre-lint:
	@ $(OCI_BUILDER) pull $(OCI_LINTER)
oci-do-lint:
	@. $(OCI_SUPPORT) ; OCI_BUILDER=$(OCI_BUILDER) \
	OCI_LINTER=$(OCI_LINTER) \
	OCI_IMAGE_FILE_PATH=$(OCI_IMAGE_FILE_PATH) \
	ociImageLint "$(OCI_IMAGES)"
oci-post-lint:
	@ $(OCI_BUILDER) rmi $(OCI_LINTER)
oci-lint: oci-pre-lint oci-do-lint oci-post-lint


.PHONY: oci-pre-build oci-do-build oci-post-build oci-build-all
oci-pre-build:
	@echo $(TAG)
oci-do-build:
	@echo "oci-do-build:Building image: $(OCI_IMAGE) registry: $(OCI_REGISTRY_HOST) context: $(OCI_IMAGE_BUILD_CONTEXT)"
	@. $(OCI_SUPPORT) ; $(OCI_BUILD_FLAGS) ociImageBuild "$(strip $(OCI_IMAGE))"
oci-post-build:
	@echo "oci-post-build:Inspecting image: $(OCI_IMAGE) registry: $(OCI_REGISTRY_HOST) context: $(OCI_IMAGE_BUILD_CONTEXT)"
	@. $(OCI_SUPPORT) ; $(OCI_BUILD_FLAGS)	ociImageInspect "$(strip $(OCI_IMAGE))"
oci-build: oci-pre-build oci-do-build oci-post-build
oci-build-all:
	$(foreach ociimage,$(OCI_IMAGES), make oci-build OCI_REGISTRY_HOST=$(OCI_REGISTRY_HOST) OCI_IMAGE=$(ociimage); rc=$$?; if [[ $$rc -ne 0 ]]; then exit $$rc; fi;)


.PHONY: oci-pre-scan oci-do-scan oci-post-scan
oci-pre-scan: oci-post-build
oci-do-scan:
	. $(OCI_SUPPORT) ; $(OCI_BUILD_FLAGS) \
	OCI_SCANNER=$(OCI_SCANNER) \
	OCI_SCANNER_FLAGS=$(OCI_SCANNER_FLAGS) \
	ociImageScan "$(OCI_IMAGES)" > REPORT.tmp
oci-post-scan:
	cat REPORT.tmp
oci-scan: oci-pre-scan oci-do-scan oci-post-scan


.PHONY: oci-pre-exec oci-do-exec oci-post-exec
oci-pre-exec:
oci-do-exec:
oci-post-exec:
oci-exec: oci-pre-exec oci-do-exec oci-post-exec


.PHONY: oci-pre-publish oci-do-publish oci-post-publish
oci-pre-publish: oci-post-build
oci-do-publish:
	@. $(OCI_SUPPORT) ; $(OCI_BUILD_FLAGS) \
	OCI_SKIP_PUSH=${OCI_SKIP_PUSH} ociImagePublish "$(OCI_IMAGES)"
oci-post-publish:
oci-publish: oci-pre-publish oci-do-publish oci-post-publish

.PHONY: oci-pre-clean oci-do-clean oci-post-clean
oci-pre-clean: oci-post-clean
oci-do-clean:
	@. $(OCI_SUPPORT) ; $(OCI_BUILD_FLAGS) \
	ociImageClean "$(OCI_IMAGES)"
oci-post-clean:
oci-clean: oci-pre-clean oci-do-clean oci-post-clean